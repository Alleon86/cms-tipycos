<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  tipos_inmueble Lang - Spanish
*
* Author: Ángel Luis Berasuain Ruiz
*         angel.berasuain@gmail.com
*         @klaimir
*
* Location: http://github.com/klaimir/openrs/
*
*
* Description:  Spanish language file for tipos_inmueble
*
*/

// Errors
$lang['tipos_inmueble_heading'] = 'Tipos Inmueble';
$lang['tipos_inmueble_label_nombre_tipo'] = 'Nombre';
$lang['tipos_inmueble_btn_insert'] = 'Insertar';
$lang['tipos_inmueble_btn_edit'] = 'Editar';
